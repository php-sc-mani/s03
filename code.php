<?php 

class Person {
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($fname,$mname,$lname){
		$this->fname = $fname;
		$this->mname = $mname;
		$this->lname = $lname;
	}

	public function printName(){
		return "Your full name is $this->fname $this->mname $this->lname";
	}
}

class Developer extends Person{
	public function printName(){
		return "Your name is $this->fname $this->mname $this->lname and you are a developer";
	}
}

class Engineer extends Person{
	public function printName(){
		return "Your are an engineer named $this->fname $this->mname $this->lname";
	}
}

$person = new Person('Senku',' ','Ishigami');
$developer = new Developer ('John', 'Finch', 'Smith');
$engineer = new Engineer('Harold', 'Myers', 'Reese');



